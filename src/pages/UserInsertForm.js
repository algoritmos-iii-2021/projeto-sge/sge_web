import React, { useState } from "react";
import { useForm } from "react-hook-form";
import moment from "moment";

import Api from "../api/api";

const AddUser = () => {
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm();

  const onSubmit = async (data, event) => {
    event.preventDefault();
    let { name, phone, email, birth, cpf, admin, password, address } = data;

    //    birth = birth.replace(/\//gi, "")
    birth = birth.split("/");
    birth = birth[1] + "/" + birth[0] + "/" + birth[2];
    birth = moment(birth).format();

    // phone = phone
    //   .replace("(", "")
    //   .replace(")", "")
    //   .replace(" ", "")
    //   .replace("-", "");

    if (admin === true) {
      admin = true;
    } else {
      admin = false;
    }

    const insertData = {
      name: name,
      password: password,
      address: address,
      phone: phone,
      email: email,
      birth: birth,
      cpf: cpf,
      admin: admin,
    };

    console.log(insertData);

    await Api.post("/employee", insertData);

    event.target.reset();
  };

  return (
    <div className="container client-form">
      <h3 className="mt-1">Dados do Funcionário</h3>
      <form onSubmit={handleSubmit(onSubmit, window.event)}>
        <div className="col-sm-12 mt-5 d-flex align-items-center">
          <label className="col-sm-2">Nome:</label>
          <input
            className="col-sm-3"
            placeholder="name"
            {...register("name")}
          />
        </div>
        <div className="col-sm-12 mt-2 d-flex align-items-center">
          <label className="col-sm-2">Email:</label>
          <input
            className="col-sm-3"
            placeholder="email"
            {...register("email")}
          />
        </div>
        <div className="col-sm-12 mt-2 d-flex align-items-center">
          <label className="col-sm-2">Senha:</label>
          <input
            className="col-sm-2"
            placeholder="password"
            type="password"
            {...register("password")}
          />
        </div>
        <div className="col-sm-12 mt-2 d-flex align-items-center">
          <label className="col-sm-2">Endereço:</label>
          <input
            className="col-sm-3"
            placeholder="address"
            {...register("address")}
          />
        </div>
        <div className="col-sm-12 mt-2 d-flex align-items-center">
          <label className="col-sm-2">CPF:</label>
          <input className="col-sm-2" placeholder="cpf" {...register("cpf")} />
        </div>
        <div className="col-sm-12 mt-2 d-flex align-items-center">
          <label className="col-sm-2">Telefone:</label>
          <input
            className="col-sm-2"
            placeholder="phone"
            {...register("phone")}
          />
        </div>
        <div className="col-sm-12 mt-2 d-flex align-items-center">
          <label className="col-sm-2">Data Nasc:</label>
          <input
            className="col-sm-2"
            placeholder="birth"
            {...register("birth")}
          />
        </div>
        <div className="col-sm-12 mt-2 d-flex align-items-center">
          <label className="col-sm-2">Status:</label>
          <select className="col-sm-2" {...register("status")}>
            <option value="false">Funcionário</option>
            <option value="true">Administrador</option>
          </select>
        </div>
        <button className="button-form rounded mt-5 mr-5" type="submit">
          Limpar
        </button>
        <button className="button-form rounded mt-5 ml-5" type="submit">
          Incluir
        </button>
      </form>
    </div>
  );
};

export default AddUser;
