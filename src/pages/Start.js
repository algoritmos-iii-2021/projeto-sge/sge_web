import React, { useEffect, useState } from "react";
import moment from "moment";

import Api from "../api/api";

const ListVehicles = () => {
  const [list, setList] = useState();

  moment.locale("pt-br");

  const getList = async () => {
    try {
      const vehicleList = await Api.get("parkingSpotHasVehicle");
      console.log(vehicleList.data);
      setList(vehicleList.data);
    } catch (err) {
      console.error(err);
    }
  };

  useEffect(() => {
    getList();
  }, []);

  return (
    <div className="col-sm-12">
      <table className="table">
        <thead>
          <tr>
            <th>Modelo</th>
            <th>Cor</th>
            <th>Placa</th>
            <th>H.Entrada</th>
          </tr>
        </thead>
        <tbody>
          {list &&
            list.map((elem) => (
              <tr key={elem.id}>
                <td>{elem.vehicleId.model}</td>
                <td>{elem.vehicleId.color}</td>
                <td>{elem.vehicleId.license}</td>
                <td>{moment(elem.checkIn).format("DD/MM/YYYY HH:mm")}</td>
              </tr>
            ))}
        </tbody>
      </table>
    </div>
  );
};

export default ListVehicles;
