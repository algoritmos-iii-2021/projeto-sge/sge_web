import React, { useEffect, useState } from "react";
import moment from "moment";
import { Link } from "react-router-dom";

import Api from "../api/api";

import { useMenu } from "../context/MenuContext"
import { Icon, InlineIcon } from '@iconify/react';
import personFill from '@iconify-icons/bi/person-fill';
import personPlusFill from '@iconify-icons/bi/person-plus-fill'

import "./perslonList.css"

const PersonList = () => {
    const [list, setList] = useState();
    const {state, setState} = useMenu();

    moment.locale("pt-br");

    let clientOrEmployee = ""    
    if (state === "funcAdd") {
      clientOrEmployee = "funcAddForm"
      } else {
      clientOrEmployee = "clientForm"
      } 

    const getList = async () => {
      if (state === "insertClient" ) { 
        try {
          const personList = await Api.get("client");
          setList(personList.data);
        } catch (err) {
          console.error(err);
        }
      } else {
        try {
          const personList = await Api.get("employee");
          setList(personList.data);
        } catch (error) {
          console.error(error);          
        }
      }

      
    };

  console.log("state person ", state)

    useEffect(() => {
        getList();
      }, []);


    return(
        <div className="container">
          <div className="head row col-sm-12 d-flex justify-content-between ">
          <div className="col-sm-3  mt-3" > 
             <Icon icon={personPlusFill} width="50" height="50" onClick={() => {setState(clientOrEmployee)}}/>                  
            </div>
            <div className="col-sm-5  mt-3"> 
            <h2>aqui vai a busca</h2>
            </div>
          </div>
            <div className="col-sm-12 d-flex justify-content-start flex-wrap mt-5" >
              {list && list.map((el) => (
                <div className="card mt-2 d-flex justify-content-center mr-3" style={{width: 160}}>
                  <div className="d-flex justify-content-center">
                    <Icon icon={personFill} width="100" height="100" />
                  </div>
                  <div className="card-body d-flex justify-content-center">
                    <a href="#" className="btn btn-secondary stretched-link">{el.name}</a>
                  </div>
                </div>
              ))}              
            </div>            
        </div>
    )
}

export default PersonList;