import React, { useState } from "react";
import { useForm } from "react-hook-form";

import { useMenu } from "../context/MenuContext";
// import "./reports.css";
import Api from "../api/api";

const Report = () => {
  const { setState } = useMenu();
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm();

  return (
    <div className="container">
      <div className="tollbar mt-1">
        <h2>Relatórios</h2>
      </div>
      <div className="buttons mt-5 col-sm-12">
        <button className="button-form mr-2 rounded" type="submit">
          Funcionários
        </button>
        <button className="button-form rounded" type="submit">
          Inadimplência
        </button>
        <div className="col-sm-12 mt-3 ">
          <button className="button-form mr-2 rounded" type="submit">
            Permanência
          </button>
          <button className="button-form rounded" type="submit">
            Financeiro
          </button>
        </div>
        <div className="mt-5">
          <h3>Filtros</h3>
          <label className="rounded"></label>
          <input
            placeholder="Data Inicial"
            className="rounded border"
            {...register("boxQtd")}
          />
          <label className=""></label>
          <input
            placeholder="Data Final"
            className="rounded border"
            {...register("boxQtd")}
          />
          <input type="submit" value="Ok" />
        </div>
      </div>
    </div>
  );
};

export default Report;
