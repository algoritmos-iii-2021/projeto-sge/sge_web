import React, { useEffect, useState, useContext } from "react";
import { UsrContext } from "../UserContext";
import { useMenu } from "../context/MenuContext";
import "./boxScreen.css";
import Api from "../api/api";

const BoxButtons = () => {
  const { setState } = useMenu();
  const dataId = useContext(UsrContext);
  const [list, setList] = useState();

  const setDataId = (id) => {
    dataId.setData(id);
  };

  const getList = async () => {
    try {
      const vehicleList = await Api.get("parkingSpotHasVehicle");
      console.log(vehicleList.data);
      setList(vehicleList.data);
    } catch (err) {
      console.error(err);
    }
  };

  useEffect(() => {
    getList();
  }, []);

  return (
    <div className="container">
      <div className="tollbar">
        <h2>Box</h2>
      </div>
      {list &&
        list.map((elem) => (
          <button
            className={
              elem.vehicleId.category === "pernoite"
                ? "btn btn-success btn-lg mt-2 ml-2 custom rounded"
                : "btn btn-dark btn-lg mt-2 ml-2 custom rounded" |
                  (elem.vehicleId.category === "hora")
                ? "btn btn-info btn-lg mt-2 ml-2 custom rounded"
                : "btn btn-dark btn-lg mt-2 ml-2 custom rounded"
            }
            key={elem.id}
            type="submit"
            onClick={() => {
              setDataId(elem.id);
              setState("boxForm");
            }}
          >
            {elem.parkingSpotId.name}
          </button>
        ))}
    </div>
  );
};

export default BoxButtons;
