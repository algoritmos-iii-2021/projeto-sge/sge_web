import React, { useState } from "react";
import { useForm } from "react-hook-form";

import { useMenu } from "../context/MenuContext";
import "./configurations.css";
import Api from "../api/api";

const Configurations = () => {
  const { setState } = useMenu();
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm();

  return (
    <div className="container">
      <div className="tollbar">
        <h2>Configurações</h2>
      </div>
      <div className="registrations mt-5">
        <h3>Cadastros</h3>
        <button
          className="button-form mr-3 rounded"
          type="submit"
          onClick={() => {
            setState("funcAdd");
          }}
        >
          Funcionários
        </button>
        <button
          className="button-form rounded"
          type="submit"
          onClick={() => {
            setState("lib");
          }}
        >
          Liberações
        </button>
      </div>
      <div className="time mt-5">
        <h3>Hora</h3>
        <label className="mr-2 ">Motos: </label>
        <input
          placeholder="Valor Hora"
          className="mr-3 rounded border"
          {...register("motorcycle")}
        />

        <label className="mr-2">Carros: </label>
        <input
          placeholder="Valor Hora"
          className="mr-3 rounded border"
          {...register("cars")}
        />

        <label className="mr-2">Camionetes: </label>
        <input
          placeholder="Valor Hora"
          className="mr-3 rounded border"
          {...register("trucks")}
        />
        <div>
          <button
            className="button-form mt-3 rounded"
            type="submit"
            onClick={() => {
              setState("lib");
            }}
          >
            Alterar
          </button>
        </div>
      </div>
      <div className="vacancy mt-5">
        <h3>Vagas</h3>
        <label className="mr-2">Quantidade de Box Disponível: </label>
        <input
          placeholder="Quantidade de box"
          className="rounded border"
          {...register("boxQtd")}
        />
        <input type="submit" value="Ok" />
      </div>
    </div>
  );
};

export default Configurations;
