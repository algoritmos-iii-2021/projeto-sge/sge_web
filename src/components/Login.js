import React from "react";
import { Form, Input, Button, Checkbox } from "antd";
import logo from "../assets/iconSmall.png";
import { Redirect, Route, useHistory } from "react-router-dom";
import Menu from "./Menu";

import "./login.css";

const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};

const Login = () => {
  const onFinish = (values) => {
    console.log("Success:", values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const history = useHistory();
  const onLogin = () => {
    history.push("/menu");
  };

  return (
    <div className="container1">
      <div className="logo">
        <img src={logo} alt="Logo" />
      </div>
      <div className="form">
        <Form
          {...layout}
          name="basic"
          initialValues={{
            remember: true,
          }}
          // onFinish={onFinish}
          // onFinishFailed={onFinishFailed}
        >
          <Form.Item
            name="username"
            // rules={[
            //   {
            //     required: true,
            //     message: "Please input your username!",
            //   },
            // ]}
          >
            <input
              type="text"
              className="input-form rounded border"
              placeholder="Login"
            />
          </Form.Item>

          <Form.Item
            name="password"
            // rules={[
            //   {
            //     required: true,
            //     message: "Please input your password!",
            //   },
            // ]}
          >
            <input
              type="password"
              className="input-form rounded border"
              placeholder="Senha"
            />
          </Form.Item>
        <div className="div-button">
          <button className="button-form rounded" onClick={() => onLogin()}>
            Entrar
          </button>
        </div>
        </Form>
      </div>
    </div>
  );
};

export default Login;
