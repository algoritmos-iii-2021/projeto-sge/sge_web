import React, { useEffect } from "react";
import { Layout, Menu } from "antd";
import {
  DesktopOutlined,
  DollarCircleOutlined,
  BarChartOutlined,
  CarOutlined,
  UserAddOutlined,
  SettingOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import { useState } from "react";
import logo from "../assets/iconSmall.png";
import "./Menu.css";

import { useMenu } from "../context/MenuContext";

import {
  StartScreen,
  ClientData,
  PersonLists,
  PersonForm,
  Configuration,
  UserAdd,
  Cashier,
  Reports,
  BoxForm,
  BoxScreen,
} from "../utils/MenuContext";

const { Sider } = Layout;

function SideMenu() {
  const [collapsed, setCollapsed] = useState(false);
  const { state, setState } = useMenu();

  const renderPG = () => {
    if (state === "start") {
      return StartScreen();
    } else if (state === "clientAdd") {
      return ClientData();
    } else if (state === "insertClient") {
      return PersonLists();
    } else if (state === "clientForm") {
      return PersonForm();
    } else if (state === "config") {
      return Configuration();
    } else if (state === "funcAdd") {
      return PersonLists();
    } else if (state === "funcAddForm") {
      console.log("clickou");
      return UserAdd();
    } else if (state === "cashier") {
      return Cashier();
    } else if (state === "reports") {
      return Reports();
    } else if (state === "boxForm") {
      return BoxForm();
    } else if (state === "boxScreen") {
      return BoxScreen();
    }
  };

  useEffect(() => {
    renderPG();
  }, []);

  return (
    <div className="container-fluid col-md-12">
      <Layout style={{ minHeight: "100vh" }}>
        <Sider collapsible collapsed={collapsed} onCollapse={setCollapsed}>
          {/* <div className="logo">
          <img src={logo} alt="Logo" />
        </div>
        <div className="welcome">
          <h4 className="title">
            Bem Vindo <br></br>Username
          </h4>
        </div> */}
          <Menu theme="dark" defaultSelectedKeys={["1"]} mode="inline">
            <Menu.Item
              key="1"
              onClick={() => {
                setState("start");
              }}
              icon={<DesktopOutlined />}
            >
              T. Inicial
            </Menu.Item>

            <Menu.Item
              key="2"
              icon={<DollarCircleOutlined />}
              onClick={() => {
                setState("cashier");
              }}
            >
              Caixa
            </Menu.Item>
            <Menu.Item
              key="3"
              icon={<UserAddOutlined />}
              onClick={() => {
                setState("insertClient");
              }}
            >
              Cad. Clientes
            </Menu.Item>
            <Menu.Item
              key="8"
              icon={<CarOutlined />}
              onClick={() => {
                setState("boxScreen");
              }}
            >
              Vagas
            </Menu.Item>
            <Menu.Item
              key="9"
              icon={<BarChartOutlined />}
              onClick={() => setState("reports")}
            >
              Relatórios
            </Menu.Item>
            <Menu.Item
              key="10"
              icon={<SettingOutlined />}
              onClick={() => {
                setState("config");
              }}
            >
              Configurações
            </Menu.Item>
            <Menu.Item key="11" icon={<LogoutOutlined />}>
              Sair
            </Menu.Item>
          </Menu>
        </Sider>
        {renderPG()}
      </Layout>
    </div>
  );
}

export default SideMenu;
